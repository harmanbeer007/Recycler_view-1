package agency.brandncodes.recylerviewexample;

public class MyPojo {
    String AuthorName;
    Integer AuthorImage;

    public MyPojo() {
    }

    public MyPojo(String authorName, Integer authorImage) {
        AuthorName = authorName;
        AuthorImage = authorImage;
    }

    public String getAuthorName() {
        return AuthorName;
    }

    public void setAuthorName(String authorName) {
        AuthorName = authorName;
    }

    public Integer getAuthorImage() {
        return AuthorImage;
    }

    public void setAuthorImage(Integer authorImage) {
        AuthorImage = authorImage;
    }
}
