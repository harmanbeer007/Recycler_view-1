package agency.brandncodes.recylerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyler;
    ArrayList<MyPojo> dabbaObj = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyler = findViewById(R.id.myRecyler);


        MyRecylerAdapter adapter = new MyRecylerAdapter(MainActivity.this, Arun());
        recyler.setLayoutManager(new GridLayoutManager(MainActivity.this, 3));
        recyler.setAdapter(adapter);

    }

    public ArrayList<MyPojo> Arun() {

        MyPojo khana1Obj = new MyPojo("aashu", R.drawable.ic_launcher_background);
        MyPojo khana1Obj1 = new MyPojo("vishu", R.drawable.ic_launcher_background);
        MyPojo khana1Obj2 = new MyPojo("hj", R.drawable.ic_launcher_background);
        MyPojo khana1Obj3 = new MyPojo("rahul", R.drawable.ic_launcher_background);
        MyPojo khana1Obj4 = new MyPojo("aman", R.drawable.ic_launcher_background);
        MyPojo khana1Obj5 = new MyPojo("puneet", R.drawable.ic_launcher_background);
        MyPojo khana1Obj6 = new MyPojo("manish", R.drawable.ic_launcher_background);
        MyPojo khana1Obj7 = new MyPojo("mehak", R.drawable.ic_launcher_background);
        dabbaObj.add(khana1Obj);
        dabbaObj.add(khana1Obj1);
        dabbaObj.add(khana1Obj2);
        dabbaObj.add(khana1Obj3);
        dabbaObj.add(khana1Obj4);
        dabbaObj.add(khana1Obj5);
        dabbaObj.add(khana1Obj6);
        dabbaObj.add(khana1Obj7);


        return dabbaObj;

    }
}
