package agency.brandncodes.recylerviewexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyRecylerAdapter extends RecyclerView.Adapter<MyRecylerAdapter.MyViewHolder> {
    private ArrayList<MyPojo> MyList;
    private Context context;
    private LayoutInflater layoutInflater;

    public MyRecylerAdapter() {
    }

    public MyRecylerAdapter(Context context, ArrayList<MyPojo> pojo) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.MyList = pojo;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.jadu, null, false);
        MyRecylerAdapter.MyViewHolder myViewHolder = new MyRecylerAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imageView.setImageResource(MyList.get(position).AuthorImage);
        holder.textView.setText(MyList.get(position).AuthorName);

    }

    @Override
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final TextView textView;

        public MyViewHolder(View viewOfMyLayout) {
            super(viewOfMyLayout);
            imageView = viewOfMyLayout.findViewById(R.id.kudiImageView);
            textView = viewOfMyLayout.findViewById(R.id.companyName);

        }
    }

}
